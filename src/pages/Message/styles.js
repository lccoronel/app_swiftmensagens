import styled from 'styled-components/native';
import Constants from 'expo-constants';
import { RectButton } from 'react-native-gesture-handler';
import { darken } from 'polished';
import Modal from 'react-native-modal';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #47453b;
  padding-bottom: 150px;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Title = styled.Text`
  font-size: 50px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Back = styled(RectButton)``;

export const Search = styled.View`
  flex-direction: row;
  margin-top: 10%;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
  autoCorrect: false,
})`
  background: ${darken(0.1, '#47453b')};
  height: 45px;
  flex: 1;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const Button = styled.TouchableHighlight`
  background: #ffb800;
  height: 45px;
  width: 30%;
  align-items: center;
  justify-content: center;
  border-top-right-radius: 4px;
`;

export const TextButton = styled.Text`
  font-weight: bold;
  font-size: 16px;
`;

export const Order = styled.View`
  background: ${darken(0.1, '#ffb800')};
  margin-top: 17px;
  padding: 15px;
`;

export const TitleOrder = styled.Text`
  font-weight: bold;
  font-size: 16px;
  color: #47453b;
`;

export const ResultOrder = styled.Text`
  font-size: 16px;
  color: #47453b;
`;

export const InputForm = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
})`
  background: ${darken(0.1, '#47453b')};
  height: 45px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const InputFormObs = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
})`
  background: ${darken(0.1, '#47453b')};
  height: 90px;
  margin-top: 5%;
  padding: 10px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const DivModal = styled(RectButton)`
  background: ${darken(0.1, '#47453b')};
  height: 45px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const TextModal = styled.Text`
  color: #ffb800;
  height: 20px;
  margin-top: 9px;
`;

export const ModalMsg = styled(Modal)``;

export const ViewModal = styled.View`
  height: 400px;
  margin: 0 35px;
  background: #47453b;
  padding: 0 15px;
  margin: 10px 0;
  justify-content: center;
`;

export const TitleModal = styled.Text`
  font-weight: bold;
  font-size: 20px;
  margin-bottom: 30px;
  text-align: center;
  color: #ffb800;
`;

export const Option = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  background: #ffb800;
  width: 89%;
  height: 11%;
  margin-top: 10px;
  align-self: center;
`;

export const Mensagens = styled.Text`
  font-size: 15px;
  text-align: center;
  color: #47453b;
  font-weight: bold;
`;

export const InputFormMsg = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
})`
  background: ${darken(0.1, '#47453b')};
  height: 85px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const Div = styled.View`
  flex-direction: row;
  margin: 10px 5px 150px;
  justify-content: space-between;
`;

export const ButtonFooter = styled.TouchableOpacity`
  background: #ffb800;
  height: 45px;
  width: 40%;
  align-items: center;
  justify-content: center;
  border-top-right-radius: 4px;
  align-self: center;
  margin-top: 20px;
`;
