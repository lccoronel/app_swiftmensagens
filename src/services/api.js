import axios from 'axios';

const api = axios.create({
  baseURL: 'http://ec2-34-238-172-194.compute-1.amazonaws.com',
});

export default api;
