import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './pages/Login';
import QrCode from './pages/QrCode';
import Message from './pages/Message';

const AppStack = createStackNavigator();

export default function Routes() {
  return (
    <NavigationContainer>
      <AppStack.Navigator screenOptions={{ headerShown: false }} mode="modal">
        <AppStack.Screen name="Login" component={Login} />
        <AppStack.Screen name="QrCode" component={QrCode} />
        <AppStack.Screen name="Message" component={Message} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
}
