/* eslint-disable camelcase */
/* eslint-disable import/no-extraneous-dependencies */
import React, { useState, useEffect } from 'react';
import { ActivityIndicator, AsyncStorage } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';

import { toastError, toastSucess } from '../../components/Toastify';
import searchOrder from '../../services/searchOrder';
import messages from '../../services/messages';
import {
  Container,
  Title,
  Header,
  Back,
  Search,
  Input,
  Button,
  TextButton,
  Order,
  TitleOrder,
  ResultOrder,
  InputForm,
  InputFormObs,
  ModalMsg,
  DivModal,
  TextModal,
  ViewModal,
  TitleModal,
  Option,
  Mensagens,
  Div,
  ButtonFooter,
} from './styles';

const Message = () => {
  const navigation = useNavigation();

  const [order, setOrder] = useState('');
  const [loadingSearch, setLoadingSearch] = useState(false);
  const [show, setShow] = useState(false);
  const [result, setResult] = useState('');
  const [visibleModal, setVisibleModal] = useState(false);
  const [msgSelect, setMsgSelect] = useState('Escolha o tipo da mensagem');
  const [msg, setMsg] = useState('');
  const [phone, setPhone] = useState('');
  const [loadingSend, setLoadingSend] = useState(false);

  useEffect(() => {
    async function getOrder() {
      const orderId = await AsyncStorage.getItem('NumOrder');
      setOrder(orderId);
    }

    getOrder();
  }, []);

  async function handleSearchOrder() {
    setLoadingSearch(true);
    const response = await searchOrder(order);

    if (response) {
      setResult(response);
      setShow(true);
      setPhone(response.phone);
      setLoadingSearch(false);
    }
    setLoadingSearch(false);
  }

  function handleSelectMessage(id, title) {
    const message = messages[id];
    setMsgSelect(title);
    setMsg(message.msg);
    setVisibleModal(!visibleModal);
  }

  async function sendSMS() {
    setLoadingSend(true);
    try {
      const response = await axios.post(
        'https://mc-51700f99pg5-gdsjbqlw9zz94.auth.marketingcloudapis.com/v2/token',
        {
          grant_type: 'client_credentials',
          client_id: '56xfho4xm6nj74m1x7yl7vt9',
          client_secret: 'O9OHc7hXYMLSgKs4ZoH7YZPq',
          scope: 'sms_read sms_write sms_send',
          account_id: '7225064',
        }
      );

      const { access_token } = response.data;

      const id = await axios.post(
        'https://mc-51700f99pg5-gdsjbqlw9zz94.rest.marketingcloudapis.com/sms/v1/messageContact/MTE5NDo3ODow/send',
        {
          mobileNumbers: [phone.replace('+', '')],
          Subscribe: true,
          Resubscribe: true,
          Override: true,
          keyword: 'SWIFT',
          shortCode: '26900',
          messageText: msg,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${access_token}`,
          },
        }
      );

      console.log(id.data);
      toastSucess('SMS enviado com suceeso');
      setLoadingSend(false);
      setShow(false);
      setPhone('');
      setMsg('');
      setMsgSelect('Escolha o tipo da mensagem');
      setOrder('');
    } catch (err) {
      toastError('Mensagem não enviada, tente novamente');
      setLoadingSend(false);
    }
  }

  return (
    <Container keyboardShouldPersistTaps="handled">
      <Header>
        <Title>Mensagem</Title>
        <Back onPress={() => navigation.navigate('Login')}>
          <FontAwesome5
            name="power-off"
            size={20}
            color="#ffb800"
            style={{ marginRight: 10 }}
          />
        </Back>
      </Header>

      <Search>
        <Input
          placeholder="Numero do pedido"
          autoCorrect={false}
          value={order}
          onChangeText={(value) => setOrder(value)}
        />
        <Button loadingSearch={loadingSearch} onPress={handleSearchOrder}>
          {loadingSearch ? (
            <ActivityIndicator color="#47453b" />
          ) : (
            <TextButton>Confirmar</TextButton>
          )}
        </Button>
      </Search>

      {show === false ? (
        <></>
      ) : (
        <Order>
          <TitleOrder>Documento</TitleOrder>
          <ResultOrder>{result.document}</ResultOrder>
          <TitleOrder>Nome</TitleOrder>
          <ResultOrder>{result.name}</ResultOrder>
          <TitleOrder>Pedido</TitleOrder>
          <ResultOrder>{result.orderId}</ResultOrder>
          <TitleOrder>Telefone</TitleOrder>
          <ResultOrder>{result.phone}</ResultOrder>
        </Order>
      )}

      <InputForm
        placeholder="Telefone (+5511900000000)"
        value={phone}
        onChangeText={(value) => setPhone(value)}
      />

      <DivModal onPress={() => setVisibleModal(true)}>
        <TextModal>{msgSelect}</TextModal>
      </DivModal>
      <ModalMsg
        isVisible={visibleModal}
        onBackdropPress={() => setVisibleModal(!visibleModal)}
        animationIn="slideInUp"
      >
        <ViewModal>
          <TitleModal>Escolher Mensagem</TitleModal>
          {messages.map((message) => (
            <Option
              onPress={() => handleSelectMessage(message.id, message.title)}
              key={message.id}
            >
              <Mensagens>{message.title}</Mensagens>
            </Option>
          ))}
          <Button
            onPress={() => setVisibleModal(!visibleModal)}
            style={{ alignSelf: 'center', marginTop: 30 }}
          >
            <TextButton>Voltar</TextButton>
          </Button>
        </ViewModal>
      </ModalMsg>

      <InputFormObs
        placeholder="Mensagem"
        autoCorrect={false}
        multiline
        value={msg}
        onChangeText={(value) => setMsg(value)}
      />

      <Div>
        <ButtonFooter loadingSend={loadingSend} onPress={sendSMS}>
          {loadingSend ? (
            <ActivityIndicator color="#47453b" />
          ) : (
            <TextButton>Mensagem</TextButton>
          )}
        </ButtonFooter>
        <ButtonFooter onPress={() => navigation.navigate('QrCode')}>
          <TextButton>QrCode</TextButton>
        </ButtonFooter>
      </Div>
    </Container>
  );
};

export default Message;
