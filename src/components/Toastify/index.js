import Toast from 'react-native-tiny-toast';

export function toastSucess(msg) {
  Toast.showSuccess(msg, {
    position: Toast.position.CENTER,
    containerStyle: {
      backgroundColor: 'rgba(50, 50, 50, 0.8)',
      borderRadius: 6,
    },
    textStyle: {
      color: '#fff',
      fontWeight: 'bold',
    },
    imgStyle: {},
    mask: false,
    maskStyle: {},
    duration: 2000,
    animation: true,
  });
}

export function toastError(msg) {
  Toast.show(msg, {
    position: Toast.position.BOTTOM,
    containerStyle: {
      backgroundColor: 'rgba(50, 50, 50, 0.8)',
      borderRadius: 6,
    },
    textStyle: {
      color: '#ffb800',
      fontWeight: 'bold',
    },
    imgStyle: {},
    mask: false,
    maskStyle: {},
    duration: 2000,
    animation: true,
  });
}
