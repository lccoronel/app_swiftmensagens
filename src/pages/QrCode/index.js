import React, { useState, useEffect } from 'react';
import { Text, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
// eslint-disable-next-line import/no-extraneous-dependencies
import { FontAwesome5 } from '@expo/vector-icons';

import {
  Container,
  Scanner,
  Button,
  TextButton,
  CodeIcon,
  Div,
  Back,
  TextContainer,
  ContainerButton,
} from './styles';

const QrCode = () => {
  const navigation = useNavigation();
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    setScanned(false);
    AsyncStorage.removeItem('NumOreder');
    (async () => {
      const { status } = await Scanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  const handleBarCodeScanned = ({ data }) => {
    setScanned(true);
    navigation.navigate('Message');
    AsyncStorage.setItem('NumOrder', data);
  };

  function digCode() {
    AsyncStorage.removeItem('NumOrder');
    setScanned(true);
    navigation.navigate('Message');
  }

  return (
    <Container>
      <Scanner onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} />
      <Div>
        <Back onPress={() => navigation.navigate('Login')}>
          <FontAwesome5 name="power-off" size={20} color="#47453b" />
        </Back>
        <TextContainer>Escanear o QrCode na embalagem do pedido</TextContainer>

        <CodeIcon
          name="qrcode-scan"
          size={150}
          color="rgba(255, 255, 255, 0.5)"
        />

        <ContainerButton>
          <Button onPress={digCode}>
            <TextButton>Digitar pedido</TextButton>
          </Button>
          <Button onPress={() => setScanned(false)}>
            <TextButton>Escanear denovo</TextButton>
          </Button>
        </ContainerButton>
      </Div>
    </Container>
  );
};

export default QrCode;
