/* eslint-disable import/no-extraneous-dependencies */
import React, { useState, useRef } from 'react';
import { ActivityIndicator, AsyncStorage } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import {
  Container,
  Title,
  Label,
  Logo,
  Input,
  InputText,
  Button,
  ButtonText,
  Version,
} from './styles';
import logo from '../../assets/logo.jpeg';
import api from '../../services/api';
import { toastError } from '../../components/Toastify';

const Login = () => {
  const passwordRef = useRef();
  const navigation = useNavigation();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  async function handleLogin() {
    setLoading(true);

    try {
      const response = await api.post('/access', {
        username,
        password,
      });

      const { token } = response.data;

      await AsyncStorage.setItem('token', token);

      setLoading(false);
      setPassword('');
      navigation.navigate('QrCode');
    } catch (err) {
      toastError('Falha no login, tente novamente!');
      setLoading(false);
    }
  }

  return (
    <Container behavior="padding">
      <Logo source={logo} />

      <Title>Login</Title>

      <Label>NOME DO USUÁRIO</Label>
      <Input>
        <FontAwesome
          name="user"
          size={20}
          color="#ffb800"
          style={{ paddingLeft: 10 }}
        />
        <InputText
          autoCorrect={false}
          autoCapitalize="none"
          value={username}
          onChangeText={(value) => setUsername(value)}
        />
      </Input>

      <Label>SENHA</Label>
      <Input>
        <FontAwesome
          name="key"
          size={20}
          color="#ffb800"
          style={{ paddingLeft: 5 }}
        />
        <InputText
          secureTextEntry
          autoCapitalize="none"
          value={password}
          onChangeText={(value) => setPassword(value)}
        />
      </Input>

      <Button loading={loading} onPress={handleLogin}>
        {loading ? (
          <ActivityIndicator color="#47453b" />
        ) : (
          <ButtonText>Entrar</ButtonText>
        )}
      </Button>
      <Version>v0.9</Version>
    </Container>
  );
};

export default Login;
