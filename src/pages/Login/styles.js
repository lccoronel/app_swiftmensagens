import styled from 'styled-components/native';
import Constants from 'expo-constants';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.KeyboardAvoidingView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #47453b;
  padding-bottom: 100px;
`;

export const Logo = styled.Image`
  width: 70%;
  height: 25%;
  align-self: center;
  margin: 8% 0;
`;

export const Title = styled.Text`
  font-size: 50px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Label = styled.Text`
  font-size: 16px;
  color: #ffb800;
  margin: 4% 0;
`;

export const Input = styled.View`
  flex-direction: row;
  border-bottom-width: 1px;
  border-bottom-color: #ffb800;
  padding-bottom: 10px;
  width: 100%;
  align-items: center;
`;

export const InputText = styled.TextInput`
  color: #ffb800;
  margin-left: 15px;
  width: 100%;
  font-size: 18px;
`;

export const Button = styled(RectButton)`
  background: #ffb800;
  border-radius: 4px;
  align-self: center;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 40px;
  margin-top: 30px;
`;

export const ButtonText = styled.Text`
  color: #47453b;
  font-size: 22px;
  font-weight: bold;
`;

export const Version = styled.Text`
  text-align: center;
  color: #ffb800;
  margin-top: 10px;
  padding-bottom: 100px ;
`;
