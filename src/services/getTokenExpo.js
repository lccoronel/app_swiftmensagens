import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import axios from 'axios';
import { Alert, Platform } from 'react-native';

async function getTokenExpo(name, email, manager) {
  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );

    let finalStatus = existingStatus;

    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);

      finalStatus = status;
    }

    if (finalStatus === 'granted') {
      const token = await Notifications.getExpoPushTokenAsync();
      try {
        const data = {
          name,
          email,
          manager,
          token,
        };

        const response = await axios.post(
          'http://192.168.15.6:3333/register',
          data
        );

        console.log(response.data);
      } catch (err) {
        console.log(err);
      }
    } else {
      console.log('sem permissao');
    }
  } else {
    return Alert.alert('Nao e um dispositivo fisico');
  }

  if (Platform.OS === 'android') {
    Notifications.createChannelAndroidAsync('default', {
      name: 'default',
      sound: true,
      priority: 'max',
      vibrate: [0, 250, 250, 250],
    });
  }
}

export default getTokenExpo;
