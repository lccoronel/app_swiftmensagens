import styled from 'styled-components/native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { RectButton } from 'react-native-gesture-handler';
import QrCode from 'react-native-vector-icons/MaterialCommunityIcons';

export const Container = styled.View`
  background: #000;
  flex: 1;
  flex-direction: column;
  align-items: center;
`;

export const Scanner = styled(BarCodeScanner)`
  width: 100%;
  height: 100%;
`;

export const Div = styled.View`
  position: absolute;
  top: 20%;
`;

export const Back = styled(RectButton)`
  background: #ff8b00;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  align-self: flex-end;
  margin-bottom: 40px;
`;

export const TextContainer = styled.Text`
  background: #47453b;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 40px;
  text-align: center;
  color: #ff8b00;
  padding: 10px 10px;
`;

export const CodeIcon = styled(QrCode)`
  align-self: center;
`;

export const ContainerButton = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: 50px 0;
`;

export const Button = styled(RectButton)`
  background: #ff8b00;
  align-items: center;
  justify-content: center;
  width: 135px;
  height: 40px;
`;

export const TextButton = styled.Text`
  color: #47453b;
  font-weight: bold;
  font-size: 16px;
`;
