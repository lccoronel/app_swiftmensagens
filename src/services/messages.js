const messages = [
  {
    id: 0,
    title: 'F9 (atendimento parcial)',
    msg:
      'SWIFT: Ola! Durante o preparo do seu pedido da Swift, um item ficou indisponivel. Nao se preocure, voce nao sera cobrado.',
  },
  {
    id: 1,
    title: 'Devolução (cliente não estava)',
    msg:
      'SWIFT: Ola! Tentamos entrega do seu pedido, porem nao encontramos voce em casa. Nao se preocupe, entraremos em contato para agendar uma nova entrega.',
  },
  {
    id: 2,
    title: 'Seu pedido está saindo pra entrega',
    msg: 'SWIFT: Ola! Seu pedido ja esta a caminho!',
  },
  {
    id: 3,
    title: 'Devolução (não conseguimos entregar)',
    msg:
      'SWIFT: Ola! Infelizmente nao conseguimos entregar o seu pedido no horario que voce agendou! Faremos uma nova tentativa de entrega!',
  },
];

export default messages;
